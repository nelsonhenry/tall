<button
    {{ $attributes->merge(['class' => 'inline-flex items-center justify-center text-white text-5/6 px-3 py-2 bg-red-700 border border-red-700 border-b-red-500 rounded hover:bg-red-500 focus:outline-none focus:ring-1 focus:ring-white focus:ring-offset-0']) }}>
    {{ $slot }}
</button>
