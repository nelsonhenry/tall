<svg class="h-full w-full fill-none stroke-primary"
    preserveAspectRatio="none"
    viewBox="0 0 100 100">
    <defs>
        <rect id="clip"
            x="0"
            y="0"
            width="100"
            height="100"
            vector-effect="non-scaling-stroke" />
        <clipPath id="clip">
            <use xlink:href="#clip" />
        </clipPath>
    </defs>
    <g>
        <use xlink:href="#clip"
            clip-path="url(#clip)"
            stroke-width="2" />
        <line x1="0"
            y1="0"
            x2="100"
            y2="100"
            vector-effect="non-scaling-stroke"
            stroke-width="1" />
        <line x1="100"
            y1="0"
            x2="0"
            y2="100"
            vector-effect="non-scaling-stroke"
            stroke-width="1" />
    </g>
</svg>
