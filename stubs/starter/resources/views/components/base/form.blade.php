<form method="POST"
    action="/">

    {{-- CSRF --}}
    @csrf

    {{-- Status --}}
    <x-form.status :status="session('status')" />

    {{-- Fields --}}
    <div class="space-y-6">

        {{-- Email --}}
        <div>
            <x-form.label for="email"
                value="Email" />
            <x-form.input id="email"
                type="email"
                name="email"
                :value="old('email')"
                required
                autofocus
                autocomplete="username" />
            <x-form.error :messages="$errors->get('email')" />
        </div>

        {{-- Password --}}
        <div>
            <x-form.label for="password"
                value="Password" />
            <x-form.input id="password"
                type="password"
                name="password"
                required
                autocomplete="current-password" />
            <x-form.error :messages="$errors->get('password')" />
        </div>

        {{-- Submit --}}
        <div class="pt-4">
            <x-form.button type="submit">
                Login
            </x-form.button>
        </div>

    </div>

</form>
