<button
    {{ $attributes->merge(['class' => 'inline-flex items-center justify-center rounded border border-secondary bg-button px-3 py-2 text-5/6 hover:border-primary hover:bg-primary hover:text-white focus-visible:border-primary focus-visible:outline-none focus-visible:ring-0']) }}>
    {{ $slot }}
</button>
