<?php

// use A17\Twill\Services\MediaLibrary\ImageService;

function _locale()
{
    return app()->getLocale();
}

function _lang()
{
    return str_replace('_', '-', app()->getLocale());
}

function _route()
{
    return request()->route()->getName();
}

function _routeIs($route)
{
    return request()->routeIs($route);
}

// function _imgUrl($image, $w = null)
// {
//     if ($w === null) return ImageService::getUrl($image->uuid);
//     return ImageService::getUrl($image->uuid, ['w' => $w]);
// }
