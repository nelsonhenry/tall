import defaultTheme from 'tailwindcss/defaultTheme.js'
// import forms from '@tailwindcss/forms'
// import typography from '@tailwindcss/typography'

export default {
    content: ['./app/**/*.php', './resources/**/*.php', './resources/**/*.js'],
    corePlugins: { aspectRatio: false },
    future: { hoverOnlyWhenSupported: true },
    plugins: [
        // forms,
        // typography,
        ({ matchUtilities, theme }) => {
            matchUtilities(
                {
                    aspect: value => ({
                        '@supports (aspect-ratio: 1 / 1)': { aspectRatio: value },
                        '@supports not (aspect-ratio: 1 / 1)': {
                            '&::before': { content: '""', float: 'left', paddingTop: `calc(100% / (${value}))` },
                            '&::after': { content: '""', clear: 'left', display: 'block' },
                        },
                    }),
                },
                { values: theme('aspectRatio') }
            )
        },
    ],
    theme: {
        extend: {
            borderColor: { DEFAULT: 'currentColor' },
            borderRadius: {
                12: '3rem',
                10: '2.5rem',
                8: '2rem',
                7: '1.75rem',
                6: '1.5rem',
                5: '1.25rem',
                4: '1rem',
                3.5: '0.875rem',
                3: '0.75rem',
                2: '0.5rem',
                1: '0.25rem',
                0.5: '0.125rem',
            },
            borderWidth: { DEFAULT: '1px' },
            colors: {
                // Light
                // primary: '#000',
                // secondary: '#999',
                // accent: '#00f',
                // bgPrimary: '#fff',
                // bgSecondary: '#ccc',
                // borderPrimary: '#ccc',
                // Dark
                primary: '#ccc',
                secondary: '#666',
                accent: '#69f',
                bgPrimary: '#101010',
                bgSecondary: '#333',
                borderPrimary: '#333',
            },
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
                serif: ['serif', ...defaultTheme.fontFamily.serif],
                mono: ['monospace', ...defaultTheme.fontFamily.mono],
            },
            fontSize: {
                12: ['3rem', '3rem'], // 48/48 (1) 5xl
                10: ['2.5rem', '2.5rem'], // 40/40 (1)
                9: ['2.25rem', '2.5rem'], // 36/40 (1.11) 4xl
                8: ['2rem', '2.25rem'], // 32/36 (1.125)
                7: ['1.75rem', '2rem'], // 28/32 (1.14)
                6: ['1.5rem', '1.75rem'], // 24/28 (1.16) 2xl
                5: ['1.25rem', '1.5rem'], // 20/24 (1.2) xl
                4: ['1rem', '1.5rem'], // 16/24 (1.5) base
                3.5: ['0.875rem', '1.25rem'], // 14/20 (1.42) sm
                3: ['0.75rem', '1rem'], // 12/16 (1.33) xs
            },
            outlineWidth: { DEFAULT: '1px' },
            screens: {
                480: '480px',
                640: '640px',
                960: '960px',
                1280: '1280px',
                1440: '1440px',
                1920: '1920px',
                2560: '2560px',
            },
            transitionDuration: { DEFAULT: '250ms' },
        },
    },
}
