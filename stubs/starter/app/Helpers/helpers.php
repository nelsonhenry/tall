<?php

namespace Helpers;

// use A17\Twill\Services\MediaLibrary\ImageService;
// use A17\Twill\Image\Facades\TwillImage;

function lang()
{
    return str_replace('_', '-', app()->getLocale());
}

function routeName()
{
    return request()->route()->getName();
}

function routeIs($route)
{
    return request()->routeIs($route);
}

function fakePosts($count)
{
    return collect()->times($count, function ($index) {
        return (object) [
            'id' => $index,
            'title' => fake()->text(50),
            'subtitle' => fake()->text(50),
            'intro' => fake()->text(100),
            'description' => fake()->text(200),
            'date' => fake()->date(),
            'categories' => [
                (object) [
                    'id' => 1,
                    'slug' => 'cat-1',
                    'name' => 'cat 1',
                ],
                (object) [
                    'id' => 2,
                    'slug' => 'cat-2',
                    'name' => 'cat 2',
                ]
            ]
        ];
    });
}

// function imgUrl($image, $w = null)
// {
//     if ($w === null) return ImageService::getUrl($image->uuid);
//     return ImageService::getUrl($image->uuid, ['w' => $w]);
// }

// function gallery($post, $gallery, $preset)
// {
//     return $post
//         ->imageObjects($gallery, 'default')
//         ->map(fn ($media) => TwillImage::make($post, $gallery, $media)->preset($preset))
//         ->toArray();
// }
