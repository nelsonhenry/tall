<!DOCTYPE html>
<html lang="{{ _lang() }}"
    x-on:keydown.escape.window="handleEscape"
    x-on:resize.window="handleResize"
    x-data="app">
@include('layouts.head')

<body>
    @include('layouts.header')
    <div class="swup-transition-main flex-1"
        id="swup"
        data-route="{{ _route() }}"
        data-locale="{{ _locale() }}">
        @yield('content')
    </div>
    @include('layouts.footer')
</body>

</html>
