import defaultTheme from 'tailwindcss/defaultTheme.js'

export default {
    content: ['./app/**/*.php', './resources/**/*.php', './resources/**/*.js'],
    corePlugins: { aspectRatio: false },
    future: { hoverOnlyWhenSupported: true },
    plugins: [
        ({ matchUtilities, theme }) => {
            matchUtilities(
                {
                    aspect: value => ({
                        '@supports (aspect-ratio: 1 / 1)': { aspectRatio: value },
                        '@supports not (aspect-ratio: 1 / 1)': {
                            '&::before': { content: '""', float: 'left', paddingTop: `calc(100% / (${value}))` },
                            '&::after': { content: '""', clear: 'left', display: 'block' },
                        },
                    }),
                },
                { values: theme('aspectRatio') }
            )
        },
    ],
    theme: {
        extend: {
            borderColor: { DEFAULT: 'currentColor' },
            borderRadius: {
                12: '3rem',
                10: '2.5rem',
                8: '2rem',
                7: '1.75rem',
                6: '1.5rem',
                5: '1.25rem',
                4: '1rem',
                3.5: '0.875rem',
                3: '0.75rem',
                2: '0.5rem',
                1: '0.25rem',
                0.5: '0.125rem',
            },
            borderWidth: { DEFAULT: '1px' },
            colors: {
                // Light
                // primary: '#000',
                // secondary: '#999',
                // accent: '#00f',
                // bgPrimary: '#fff',
                // bgSecondary: '#ccc',
                // borderPrimary: '#ccc',
                // Dark
                primary: '#ccc',
                secondary: '#666',
                accent: '#69f',
                bgPrimary: '#101010',
                bgSecondary: '#333',
                borderPrimary: '#333',
            },
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
            },
            fontSize: {
                12: '3rem',
                10: '2.5rem',
                9: '2.25rem',
                8: '2rem',
                7: '1.75rem',
                6: '1.5rem',
                5: '1.25rem',
                4: '1rem',
                3.5: '0.875rem',
                3: '0.75rem',
                2: '0.5rem',
                1: '0.25rem',
                0.5: '0.125rem',
            },
            outlineWidth: { DEFAULT: '1px' },
            screens: {
                480: '480px',
                640: '640px',
                768: '768px',
                960: '960px',
                1024: '1024px',
                1280: '1280px',
                1440: '1440px',
                1920: '1920px',
                2560: '2560px',
            },
            transitionDuration: { DEFAULT: '250ms' },
        },
    },
}
