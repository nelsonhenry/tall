{{-- blade-formatter-disable --}}
<head>

    {{-- Base --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="{{ config('app.name') }}">

    {{-- SEO --}}
    <title>@yield('page_title')</title>
    <meta name="description" content="@yield('page_description')">
    <meta property="og:title" content="@yield('page_title')">
    <meta property="og:description" content="@yield('page_description')">
    <meta property="og:image" content="@yield('page_image_url')">
    <meta property="og:url" content="{{ Request::url() }}">
    {{-- {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!} --}}

    {{-- Matomo --}}
    {{-- @includeWhen(config('app.env') === 'production', 'layouts.matomo') --}}

    {{-- Favicon --}}
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    {{-- Assets --}}
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    {{-- Debug --}}
    @includeWhen(config('app.env') !== 'production', 'layouts.debug')

</head>
{{-- blade-formatter-enable --}}
