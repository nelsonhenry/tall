<header class="px-[5%] py-6">
    <div class="mx-auto max-w-screen-1920">
        <nav>
            <ul class="flex space-x-6">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('about') }}">About</a></li>
            </ul>
        </nav>
    </div>
</header>
