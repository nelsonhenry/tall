import Alpine from 'alpinejs'
import collapse from '@alpinejs/collapse'
import focus from '@alpinejs/focus'
import persist from '@alpinejs/persist'
import Swup from 'swup'
import SwupPreloadPlugin from '@swup/preload-plugin'
window.Alpine = Alpine
Alpine.plugin([collapse, focus, persist])

Alpine.data('app', () => ({
    route: '',
    locale: '',
    showMenu: false,
    isDesktop: false,
    isLoading: false,
    debug: Alpine.$persist(false),
    init() {
        this.initPage()
        const swup = new Swup({ linkToSelf: 'navigate', plugins: [new SwupPreloadPlugin()] })
        swup.hooks.on('page:view', () => this.initPage())
    },
    initPage() {
        this.route = document.querySelector('#swup').dataset.route
        this.locale = document.querySelector('#swup').dataset.locale
        this.showMenu = false
        this.handleResize()
    },
    handleEscape() {
        if (this.showMenu) this.showMenu = false
        else if (this.route === 'projects.show') history.back()
    },
    handleResize() {
        this.isDesktop = window.matchMedia('(min-width: 960px)').matches
    },
}))

Alpine.data('debug', () => ({
    breakpoints: [480, 640, 768, 960, 1024, 1280, 1440, 1920, 2560],
    windowWidth: 0,
    windowHeight: 0,
    currentBreakpoint: 0,
    currentFontSize: '',
    currentLineHeight: '',
    init() {
        this.handleResize()
        document.body.classList.toggle('debug', this.debug)
    },
    handleResize() {
        this.windowWidth = window.innerWidth
        this.windowHeight = window.innerHeight
        this.currentFontSize = getComputedStyle(document.documentElement).fontSize
        this.currentLineHeight = getComputedStyle(document.documentElement).lineHeight
        this.currentBreakpoint = this.breakpoints.reduce((prev, curr) => (this.windowWidth >= curr ? curr : prev), 'none')
    },
    handleKeydown() {
        this.debug = !this.debug
        document.body.classList.toggle('debug', this.debug)
    },
}))

Alpine.start()
