<div x-data="swiper">

    <div class="swiper"
        x-ref="carousel">

        {{-- Slides --}}
        <div class="swiper-wrapper aspect-video">
            @foreach ([1, 2, 3] as $image)
                <div class="swiper-slide"
                    data-caption="Caption {{ $image }}">
                    <x-base.image aspect="landscape" />
                </div>
            @endforeach
        </div>

        {{-- Button prev --}}
        <div class="swiper-button-prev">
            <x-svg.chevron-left />
        </div>

        {{-- Button next --}}
        <div class="swiper-button-next">
            <x-svg.chevron-right />
        </div>

        {{-- Pagination --}}
        <div class="swiper-pagination"></div>

    </div>

    {{-- Active caption --}}
    <div class="mt-4 text-center"
        x-text="activeCaption"></div>

</div>
