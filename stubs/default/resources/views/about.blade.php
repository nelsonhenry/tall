@extends('layouts.app')
@section('content')
    <main class="px-[5%]">
        <div class="mx-auto max-w-screen-1920">
            <p>Hello from <span x-text="route"></span></p>
        </div>
    </main>
@endsection
