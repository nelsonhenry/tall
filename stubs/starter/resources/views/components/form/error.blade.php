@props(['messages'])

@if ($messages)
    <ul {{ $attributes->merge(['class' => 'mt-2 inline-block text-5/6 text-red-500']) }}>
        @foreach ((array) $messages as $message)
            <li>{{ $message }}</li>
        @endforeach
    </ul>
@endif
