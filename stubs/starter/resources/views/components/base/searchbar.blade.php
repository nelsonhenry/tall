<form x-show="showSearch"
    x-trap="showSearch"
    x-transition.opacity.duration.250ms
    x-cloak>
    @csrf
    <div class="flex h-14 items-center space-x-4 bg-white px-[5vw]">

        {{-- Input --}}
        <input class="h-full w-full placeholder:text-secondary focus:outline-none"
            type="text"
            placeholder="Search" />

        {{-- Submit button --}}
        <button class="h-6 p-0.5"
            type="submit"
            aria-labelledby="Submit search">
            <x-svg.search />
        </button>

        {{-- Close search button --}}
        <button class="h-6 p-0.5"
            type="button"
            aria-labelledby="Close search"
            x-on:click="showSearch = false">
            <x-svg.cross />
        </button>

    </div>
</form>
