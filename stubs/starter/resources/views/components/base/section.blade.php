@props(['title'])

<section {{ $attributes->merge(['class' => '']) }}>
    <div>
        <h2>{{ $title }}</h2>
        @isset($info)
            <div>{{ $info }}</div>
        @endisset
    </div>
    @isset($slot)
        <div>{{ $slot }}</div>
    @endisset
</section>

{{--

<x-card title="Titre de la section" class="mb-4">
    <x-slot name="info">
        <p>Infos</p>
    </x-slot>
    <p>Contenu principal</p>
</x-card> 

--}}
