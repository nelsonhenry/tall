# Laravel TALL Preset (fork)

TALL preset for Laravel, forked from https://github.com/laravel-frontend-presets/tall.

## Installation

This preset is intended to be installed into a fresh Laravel application. Follow [Laravel's installation instructions](https://laravel.com/docs/installation) to ensure you have a working environment before continuing.

### Installation (default)

Then simply run the following commands:

```bash
composer require variable.club/tall
php artisan ui tall
npm install
npm run dev
```

### Installation (with blast)

If you would like to install the preset and its auth scaffolding in a fresh Laravel application, make sure to use the `--blast` flag on the `ui` command:

```bash
composer require area17/blast variable.club/tall
php artisan ui tall --option=blast
npm install
npm run dev
```

To run Blast, use this command:

```bash
php artisan blast:launch
```

Then, navigate to `http://localhost:6006`.

## CSS purging

Tailwind uses PurgeCSS to remove any unused classes from your production CSS builds. You can modify or remove this behaviour in the `purge` section of your `tailwind.config.js` file. For more information, please see the [Tailwind documentation](https://tailwindcss.com/docs/controlling-file-size/).

## Removing the package

If you don't want to keep this package installed once you've installed the preset, you can safely remove it. Unlike the default Laravel presets, this one publishes all the auth logic to your project's `/app` directory, so it's fully redundant.

## Credits

### Original authors

-   [Dan Harrin](https://github.com/DanHarrin)
-   [Liam Hammett](https://github.com/imliam)
-   [Ryan Chandler](https://github.com/ryangjchandler)
-   [Tailwind UI](https://tailwindui.com) for the default authentication and pagination views
-   [All Contributors](../../contributors)

### Fork author

-   [Nelson Henry](http://gitlab.com/nelsonhenry)
