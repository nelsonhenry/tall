@props(['status'])

@if ($status)
    <div {{ $attributes->merge(['class' => 'fixed bottom-0 left-0 right-0 flex justify-center px-4 py-6 text-center text-white bg-green-500']) }}
        x-data="{ show: true }"
        x-init="setTimeout(() => show = false, 3000)"
        x-show="show"
        x-transition.opacity
        x-cloak>
        {{ $status }}
    </div>
@endif
