import Swiper from 'swiper'
import { Navigation, Pagination } from 'swiper/modules'

Alpine.data('swiper', options => ({
    swiper: null,
    options,
    activeIndex: 0,
    activeCaption: '',
    init() {
        this.swiper = new Swiper(this.$refs.carousel, {
            ...this.options,
            modules: [Navigation, Pagination],
            pagination: { el: '.swiper-pagination', clickable: true },
            navigation: { prevEl: '.swiper-button-prev', nextEl: '.swiper-button-next' },
            keyboard: { enabled: true },
            loop: true,
            lazyPreloadPrevNext: 2,
            on: { init: () => this.setCaption(this.$root.querySelector('.swiper-slide')) },
        })

        this.swiper.on('slideChange', () => {
            this.activeIndex = this.swiper.realIndex
            setTimeout(() => this.setCaption(this.$root.querySelector('.swiper-slide-active')), 1)
        })
    },
    setCaption(activeSlide) {
        this.activeCaption = activeSlide.dataset.caption || ''
    },
}))