@props(['value'])

<label {{ $attributes->merge(['class' => 'inline-block text-5/6 text-secondary mb-2']) }}>
    {{ $value ?? $slot }}
</label>
