{{-- ['header' => [2, 3, 4, false]], 'bold', 'italic', 'underline', 'strike', 'link', 'blockquote', 'ordered', 'bullet', 'hr', 'table'] --}}

<article class="body">

    <h1>Header one {{ fake()->words(10, true) }}</h1>

    <p>
        Paragraph
        <strong>bold</strong>
        <em>italic</em>
        <u>underline</u>
        <s>strike</s>
        <a href="#"
            target="_blank">link</a>
        {{ fake()->words(80, true) }}
    </p>

    <hr>

    <p>{{ fake()->words(40, true) }}</p>

    <h2>Header two {{ fake()->words(10, true) }}</h2>

    <blockquote>
        <p>Blockquote {{ fake()->words(20, true) }}</p>
    </blockquote>

    <h3>Header three {{ fake()->words(10, true) }}</h3>

    <ol>
        <li>
            <p>Ordered list item {{ fake()->words(20, true) }}</p>
        </li>
        <li>
            <p>Ordered list item {{ fake()->words(20, true) }}</p>
        </li>
        <li>
            <p>Ordered list item {{ fake()->words(20, true) }}</p>
        </li>
    </ol>

    <h4>Header four {{ fake()->words(10, true) }}</h4>

    <ul>
        <li>
            <p>Bullet list item {{ fake()->words(20, true) }}</p>
        </li>
        <li>
            <p>Bullet list item {{ fake()->words(20, true) }}</p>
        </li>
        <li>
            <p>Bullet list item {{ fake()->words(20, true) }}</p>
        </li>
    </ul>

    <table>
        <thead>
            <tr>
                <th>Table header {{ fake()->words(5, true) }}</th>
                <th>Table header {{ fake()->words(5, true) }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Table data {{ fake()->words(10, true) }}</td>
                <td>Table data {{ fake()->words(10, true) }}</td>
            </tr>
            <tr>
                <td>Table data {{ fake()->words(10, true) }}</td>
                <td>Table data {{ fake()->words(10, true) }}</td>
            </tr>
            <tr>
                <td>Table data {{ fake()->words(10, true) }}</td>
                <td>Table data {{ fake()->words(10, true) }}</td>
            </tr>
        </tbody>
    </table>

</article>
