@php
    $title = 'Select a language';
    $selectedLang = 'en';
    $langs = [
        (object) ['title' => 'English', 'slug' => 'en'],
        (object) ['title' => 'Francais', 'slug' => 'fr'],
        (object) ['title' => 'Nederlands', 'slug' => 'nl'],
    ];
@endphp

<div class="relative"
    x-data="{
        selectedItem: 'fr',
        show: false,
        justify: null,
        currentIndex: -1,
        fromKeyboard: false,
        open() {
            this.show = true;
            this.currentIndex = -1;
            this.$nextTick(() => {
                this.setJustify()
                this.$refs.container.scrollTop = 0
            })
        },
        close() {
            this.show = false;
        },
        handleKeydown(e) {
            if (this.show) {
                const items = this.$root.querySelectorAll('[data-item]');
    
                e.preventDefault();
                this.fromKeyboard = true;
    
                if (e.key === 'ArrowDown' && this.currentIndex < items.length - 1) this.currentIndex++;
                else if (e.key === 'ArrowUp' && this.currentIndex > 0) this.currentIndex--;
    
                if (items[this.currentIndex]) items[this.currentIndex].focus();
            }
        },
        handleMouseMove(e) {
            const target = e.target.closest('[data-item]');
            if (target && !this.fromKeyboard) this.currentIndex = +target.getAttribute('data-item');
            this.fromKeyboard = false;
        },
        setJustify() {
            this.$refs.dropdown.style.left = 0
            this.$refs.dropdown.style.right = 'auto'
            const dropdownRect = this.$refs.dropdown.getBoundingClientRect();
            this.$refs.dropdown.style = ''
            this.justify = dropdownRect.right > window.innerWidth ? 'right' : 'left';
        }
    }"
    x-on:resize.window="close()"
    x-on:keydown.window="handleKeydown($event)"
    x-on:keydown.escape.window="if (show) close()">

    {{-- Button --}}
    <button class="flex h-8 items-center justify-center space-x-1.5 uppercase"
        x-on:click="show ? close() : open()"
        x-on:keydown.down.window="if (document.activeElement === $el) open()">
        <span class="inline-block w-6"
            x-text="selectedItem"></span>
        <span class="inline-block transition-transform"
            :class="show && '-scale-y-100'"><x-svg.chevron-down /></span>
    </button>

    {{-- Dialog --}}
    <div class="absolute top-full z-10 transition-[transform,opacity,filter]"
        :class="justify === 'right' ? 'right-0' : 'left-0'"
        x-on:click.away="close()"
        x-show="show"
        x-ref="dropdown"
        x-transition:enter-start.duration.250ms="opacity-0 translate-y-2"
        x-transition:leave-end.duration.250ms="opacity-0 blur-[1px]"
        x-cloak>

        {{-- Container --}}
        <div class="mt-3 max-h-full overflow-auto whitespace-nowrap rounded-[3px] border bg-bgPrimary shadow-md focus:outline-none"
            tabindex="-1"
            x-on:mousemove="handleMouseMove($event)"
            x-on:mouseleave="currentIndex = null"
            x-ref="container">
            <nav>
                <ul>
                    @foreach ($langs as $lang)
                        <li class="border-b last:border-b-0"
                            role="option">
                            <a class="flex h-10 items-center px-3 focus-within:outline-none"
                                data-item="{{ $loop->index }}"
                                x-on:click.prevent="selectedItem = '{{ $lang->slug }}'; close()"
                                x-on:keydown.enter="selectedItem = '{{ $lang->slug }}'; close()"
                                :class="currentIndex === {{ $loop->index }} && 'bg-bgSecondary'"
                                tabindex="-1"
                                href="#">{{ $lang->title }}</a>
                        </li>
                    @endforeach
                </ul>
            </nav>
        </div>

    </div>
</div>
