<?php

namespace App\Http\Controllers;

use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    protected $cacheDuration;

    public function __construct()
    {
        $this->cacheDuration = 48 * 60; // 48h
        parent::__construct();
    }

    /**
     * Display the home
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Cache::remember('fake-posts', $this->cacheDuration, fn () => Helpers\fakePosts(10));

        return response()->view('home', compact('posts'));
    }
}
