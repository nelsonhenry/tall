<div class="pointer-events-none font-mono text-[12px] leading-none text-[#00f] [&_*]:!outline-none"
    x-data="debug"
    x-on:resize.window="handleResize"
    x-on:keydown.;.window="handleKeydown"
    x-show="debug"
    x-cloak>

    {{-- Grid --}}
    <div class="fixed inset-0 z-[9999] mx-auto grid max-w-screen-2560 grid-cols-2 gap-6 px-x opacity-10 960:grid-cols-6">
        @for ($i = 0; $i < 6; $i++)
            <div class="{{ $i > 1 ? 'hidden 960:block' : '' }} bg-[#00f]"></div>
        @endfor
    </div>

    {{-- Current sizes --}}
    <div class="fixed bottom-0 left-0 z-[9999] bg-white p-[2px] uppercase">
        <p x-text="`${windowWidth}x${windowHeight}·${currentBreakpoint}·${currentFontSize.replace('px','')}/${currentLineHeight}`"></p>
    </div>

    {{-- Breakpoints --}}
    <template x-for="breakpoint in breakpoints">
        <div class="fixed bottom-0 z-[9999] h-3"
            :class="currentBreakpoint === breakpoint && 'font-bold'"
            :style="`left: ${breakpoint}px`"
            x-cloak>
            <div class="absolute bottom-0 right-0 whitespace-nowrap border-r border-r-[#00f] bg-white p-[2px] uppercase"
                x-text="breakpoint"></div>
        </div>
    </template>

    {{-- Mouse --}}
    {{-- <div class="fixed z-[9999] w-[200vw] -translate-x-1/2 border-t-2 border-dotted border-[#00f]"
        x-on:mousemove.window="
            $el.style.top = $event.clientY + 'px';
            $el.style.left = $event.clientX + 'px';
        ">
    </div>
    <div class="fixed z-[9999] h-[200vh] -translate-y-1/2 border-l-2 border-dotted border-[#00f]"
        x-on:mousemove.window="
            $el.style.top = $event.clientY + 'px';
            $el.style.left = $event.clientX + 'px';
        ">
    </div> --}}

    {{-- Center --}}
    {{-- <div class="fixed left-1/2 top-0 z-[9999] h-screen -translate-x-px border-r-2 border-dotted border-r-[#00f]"></div>
    <div class="fixed left-0 top-1/2 z-[9999] w-full -translate-y-px border-b-2 border-dotted border-b-[#00f]"></div> --}}

</div>
