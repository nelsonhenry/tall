<svg class="overflow-visible fill-none stroke-current stroke-2"
    xmlns="http://www.w3.org/2000/svg"
    preserveAspectRatio="none"
    viewBox="0 0 16 16">
    <polyline points="16,0 0,8 16,16"
        vector-effect="non-scaling-stroke"></polyline>
</svg>
