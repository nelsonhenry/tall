@props([
    'aspect' => 'square',
    'imageStyles' => '',
    'sizes' => '100vw',
])

@php
    $aspects = [
        'landscape' => [
            'ratio' => 66.67,
            'heights' => [427, 640, 853, 1280],
        ],
        'video' => [
            'ratio' => 56.25,
            'heights' => [360, 540, 720, 1080],
        ],
        'square' => [
            'ratio' => 100,
            'heights' => [640, 960, 1280, 1920],
        ],
        'portrait' => [
            'ratio' => 133.34,
            'heights' => [960, 1440, 1920],
        ],
    ];
@endphp

<div class="twill-image-wrapper h-full w-full"
    data-twill-image-wrapper
    style="position:relative;overflow:hidden;background-color:transparent">
    <div aria-hidden="true"
        style="padding-top:{{ $aspects[$aspect]['ratio'] }}%"></div>
    <picture>
        <img data-main-image
            decoding="async"
            loading="lazy"
            srcset="
                {{ fake()->imageUrl(640, $aspects[$aspect]['heights'][0]) }} 640w,
                {{ fake()->imageUrl(960, $aspects[$aspect]['heights'][1]) }} 960w,
                {{ fake()->imageUrl(1280, $aspects[$aspect]['heights'][2]) }} 1280w
                @if ($aspect !== 'portrait') , {{ fake()->imageUrl(1920, $aspects[$aspect]['heights'][3]) }} 1920w @endif
            "
            sizes="{{ $sizes }}"
            src="{{ fake()->imageUrl(640, $aspects[$aspect]['heights'][0]) }}"
            alt="#"
            style="transform:translateZ(0px);will-change:opacity;bottom:0;height:100%;left:0;margin:0;max-width:none;padding:0;position:absolute;right:0;top:0;width:100%;object-fit:contain;object-position:center center;opacity:1;{{ $imageStyles }}">
    </picture>
</div>
