<svg class="overflow-visible fill-none stroke-current stroke-2"
    xmlns="http://www.w3.org/2000/svg"
    preserveAspectRatio="none"
    viewBox="0 0 16 16">
    <line x1="0"
        y1="0"
        x2="16"
        y2="16"
        vector-effect="non-scaling-stroke" />
    <line x1="16"
        y1="0"
        x2="0"
        y2="16"
        vector-effect="non-scaling-stroke" />
</svg>
