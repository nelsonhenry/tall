<div class="aspect-video">
    <video class="h-full w-full object-contain"
        playsinline
        autoplay
        preload="none"
        poster=""
        muted
        defaultmuted
        loop
        src="{!! $image['video_url'] !!}">
    </video>
</div>
