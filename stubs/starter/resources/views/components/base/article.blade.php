<article>

    {{-- Header --}}
    <section>

        {{-- Title --}}
        @if ($post->title)
            <h1>{{ $post->title }}</h1>
        @endif

        {{-- Subtitle --}}
        @if ($post->subtitle)
            <h2>{{ $post->subtitle }}</h2>
        @endif

        {{-- Categories --}}
        @if (count($post->categories))
            <ul class="flex flex-wrap">
                @foreach ($post->categories as $category)
                    <li>
                        <a href="{{ $category->slug }}">{{ $category->name }}</a>
                        @if (!$loop->last)
                            <span>,&nbsp;</span>
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif

    </section>

    {{-- Cover --}}
    @if ($post->hasImage('cover'))
        <section>
            <figure>
                {!! TwillImage::make($post, 'cover')->preset('full')->render(['class' => 'h-full w-full', 'imageStyles' => ['object-fit' => 'cover']]) !!}
            </figure>
        </section>
    @endif

    {{-- Intro --}}
    @if ($post->intro)
        <section class="body">
            {!! $post->intro !!}
        </section>
    @endif

    {{-- Description --}}
    @if ($post->description)
        <section class="body">
            {!! $post->description !!}
        </section>
    @endif

    {{-- Gallery --}}
    @if ($post->hasImage('gallery'))
        <section class="grid grid-cols-2 640:grid-cols-3 gap-8">
            @foreach ($post->imageObjects('gallery', 'default') as $image)
                <figure>
                    {!! TwillImage::make($post, 'gallery', $image)->preset('small')->render() !!}
                </figure>
            @endforeach
        </section>
    @endif

</article>
