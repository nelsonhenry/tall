{{-- 1 2 3 4 --}}
{{-- 480:grid-cols-2 960:grid-cols-3 1280:grid-cols-4 --}}

{{-- 1 2 3 --}}
{{-- 640:grid-cols-2 1280:grid-cols-3 --}}

{{-- 1 2 --}}
<ul class="grid items-start gap-8 960:grid-cols-2">
    @foreach ($posts as $post)
        <li class="relative">
            <figure>
                <x-base.image aspect="landscape" />
            </figure>
            <h2 class="mt-3 text-6/8">
                <a class="after:absolute after:inset-0"
                    href="#">{{ $post->title }}</a>
            </h2>
            <p class="mt-3 text-3/4">{{ $post->date }}</p>
            <h3 class="mt-3">{{ $post->subtitle }}</h3>
            <ul class="mt-4 flex flex-wrap gap-3 text-3/4">
                <li>
                    <a class="relative flex items-center border px-2 py-1"
                        href="#cat-1">Category 1</a>
                </li>
                <li>
                    <a class="relative flex items-center border px-2 py-1"
                        href="#cat-2">Category 2</a>
                </li>
            </ul>
        </li>
    @endforeach
</ul>
