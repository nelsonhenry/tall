<header class="sticky top-0 z-10 mx-auto max-w-screen-2560 bg-white px-[5%] py-8">
    <nav class="flex justify-between">

        {{-- Site title --}}
        <h1>
            <a class="inline-block"
                href="{{ route('home') }}">
                {{ config('app.name') }}
            </a>
        </h1>

        {{-- Primary nav --}}
        <ul class="flex space-x-2">
            <li>
                <a class="inline-block"
                    href="{{ route('about') }}">
                    About
                </a>
            </li>
        </ul>

    </nav>
</header>

@php($links = [(object) ['title' => 'Index', 'href' => '#'], (object) ['title' => 'About', 'href' => '#']])

<header class="fixed top-0 z-50 mx-auto w-full max-w-screen-2560 border-b bg-bgPrimary">

    {{-- Topbar --}}
    <nav class="flex h-14 items-center justify-between px-[5vw]">

        {{-- Site title --}}
        <h1 class="flex-1">
            <a class="inline-block py-1"
                href="/">{{ config('app.name') }}</a>
        </h1>

        {{-- Menu desktop --}}
        <ul class="hidden space-x-6 md:flex">
            @foreach ($links as $link)
                <li>
                    <a class="inline-block py-1"
                        href="{{ $link->href }}">{{ $link->title }}</a>
                </li>
            @endforeach
        </ul>

        {{-- Actions --}}
        <div class="flex flex-1 items-center justify-end space-x-4">

            {{-- Lang select --}}
            @include('components.base.lang-menu')

            {{-- Search open button --}}
            <button class="h-6 p-0.5"
                aria-labelledby="Open search"
                x-on:click="showSearch = true">
                <x-svg.search />
            </button>

            {{-- Menu open button --}}
            <button class="h-6 p-0.5 md:hidden"
                aria-labelledby="Open menu"
                x-on:click="showMenu = true"
                x-show="!showMenu">
                <x-svg.menu />
            </button>

            {{-- Menu close button --}}
            <button class="h-6 p-0.5 md:hidden"
                aria-labelledby="Close menu"
                x-on:click="showMenu = false"
                x-show="showMenu"
                x-cloak>
                <x-svg.cross />
            </button>

        </div>

    </nav>

    {{-- Menu mobile --}}
    <nav class="md:hidden"
        x-show="showMenu"
        x-collapse.duration.250ms
        x-cloak>
        <div class="h-[calc(100vh-3.5rem)] max-h-[calc(100vh-3.5rem)] overflow-y-auto px-[5vw]"
            :class="full ? 'pb-14' : 'pb-4'">
            <ul class="flex min-h-full flex-col px-[5vw]"
                :class="full ? 'space-y-4 justify-center items-center' : ''">
                @foreach ($links as $link)
                    <li>
                        <a class="inline-block"
                            href="{{ $link->href }}">{{ $link->title }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </nav>

    {{-- Search --}}
    <div class="absolute top-0 z-10 w-full">
        @include('components.base.searchbar')
    </div>

</header>
