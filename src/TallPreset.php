<?php

namespace LaravelFrontendPresets\Tall;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Laravel\Ui\Presets\Preset;

class TallPreset extends Preset
{
    const NPM_PACKAGES_TO_ADD = [
        '@alpinejs/collapse' => '^3',
        '@alpinejs/focus' => '^3',
        '@alpinejs/persist' => '^3',
        '@eslint/js' => '^9',
        "@swup/preload-plugin" => "^3",
        '@tailwindcss/forms' => '^0.5',
        '@tailwindcss/typography' => '^0.5',
        '@vitejs/plugin-legacy' => '^6',
        'alpinejs' => '^3',
        'eslint' => '^9',
        'globals' => '^15',
        'postcss-import' => '^16',
        'prettier' => '^3',
        'prettier-plugin-tailwindcss' => '^0.6',
        "swup" => "^4",
    ];

    const NPM_PACKAGES_TO_REMOVE = ['axios'];

    public static function install(): void
    {
        // Update packages
        static::updatePackages();

        // Filesystem
        $filesystem = new Filesystem();

        // Delete
        $filesystem->delete(resource_path('views/welcome.blade.php'));
        $filesystem->delete(resource_path('js/bootstrap.js'));

        // Clone stubs
        $filesystem->copyDirectory(__DIR__ . '/../stubs/default', base_path());

        // Autoload helpers
        static::updateFile(base_path('composer.json'), function ($file) {
            $data = json_decode($file, true);
            $data['autoload']['files'] = ['app/Helpers/helpers.php'];
            return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        });

        // Replace .gitignore content
        file_put_contents(base_path('.gitignore'), file_get_contents(__DIR__ . '/../stubs/gitignore.txt'));
    }

    public static function installAuth()
    {
        // Filesystem
        $filesystem = new Filesystem();

        // Clone stubs
        $filesystem->copyDirectory(__DIR__ . '/../stubs/auth', base_path());
    }

    protected static function updatePackageArray(array $packages)
    {
        return array_merge(
            static::NPM_PACKAGES_TO_ADD,
            Arr::except($packages, static::NPM_PACKAGES_TO_REMOVE)
        );
    }

    protected static function updateFile(string $path, callable $callback)
    {
        $originalFileContents = file_get_contents($path);
        $newFileContents = $callback($originalFileContents);
        file_put_contents($path, $newFileContents);
    }
}
