<svg class="overflow-visible fill-none stroke-current"
    xmlns="http://www.w3.org/2000/svg"
    preserveAspectRatio="none"
    viewBox="0 0 16 16">
    <line x1="10"
        y1="10"
        x2="16"
        y2="16"
        vector-effect="non-scaling-stroke" />
    <circle cx="6"
        cy="6"
        r="6"
        vector-effect="non-scaling-stroke" />
</svg>
